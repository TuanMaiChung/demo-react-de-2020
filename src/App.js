import React from "react";
import BookItem from "./components/BookItem";
import BookDetail from "./components/BookDetail";
import "./styles.css";
import books from "./books.json";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      bookList: [],
      selectingBook: null,
    };
  }

  componentDidMount() {
    const newState = {
      bookList: books,
    };

    this.setState(newState);
  }

  handleBookSelect(selectBook) {
    const newState = {
      selectingBook: selectBook,
    };

    this.setState(newState);
  }

  render() {
    return (
      <div className="row">
        {/* Left column */}
        <div className="col">
          {this.state.bookList.map((book) => {
            return (
              <div onClick={() => this.handleBookSelect(book)}>
                <BookItem key={book.id} book={book}></BookItem>
              </div>
            );
          })}
        </div>
        {/* Right column */}
        <div className="col">
          {this.state.selectingBook && (
            <BookDetail bookDetail={this.state.selectingBook}></BookDetail>
          )}
        </div>
      </div>
    );
  }
}

export default App;
