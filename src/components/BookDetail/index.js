import React, { Component } from "react";

export class BookDetail extends Component {
  render() {
    return (
      <div>
        <div className="book-details__title">
          <h1>{this.props.bookDetail.title}</h1>
        </div>
        <div className="book-details__image">
          <img src={this.props.bookDetail.imageUrl} alt="book" />
        </div>
        <div className="book-details__description">
          <p>{this.props.bookDetail.description}</p>
        </div>
      </div>
    );
  }
}

export default BookDetail;
