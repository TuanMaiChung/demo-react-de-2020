import React, { Component } from "react";

export class BookItem extends Component {
  render() {
    return (
      <div className="books__item">
        <h3>{this.props.book.title}</h3>
        <small>Author: {this.props.book.author}</small>
      </div>
    );
  }
}

export default BookItem;
